import datetime

from mongoengine import *
from nucleusapp_telegram.base import bot
from telebot import types

from app import App
from . import log
from .utils import get_username_or_name


class Chat(Document):
    chat_id = IntField(max_value=0)
    active = BooleanField(default=False)
    last_seen = DateTimeField(default=datetime.datetime.now)
    messages = IntField(default=0)
    min_messages = IntField(default=5)

    text = StringField()
    join_text = StringField()
    last_join = DateTimeField(default=None, null=True)

    access = ListField()
    access_for_chat_admins = BooleanField(default=True)

    @classmethod
    def get_chat(cls, chat_id) -> 'Chat':
        if chat_id > 0:
            # Not allowed in PM
            return None

        match = Chat.objects(chat_id=chat_id)
        if match:
            return match[0]

        chat = Chat(chat_id=chat_id)
        chat.save()
        return chat

    @classmethod
    def active_chats(cls) -> 'Chat':
        yield from Chat.objects(active=True)

    def get_admins(self) -> [types.ChatMember]:
        return bot.get_chat_administrators(self.chat_id)

    def get_admins_ids(self):
        admins = {member.user.id: member.status for member in self.get_admins()}
        root_uid = App().settings.get('TELEGRAM', {}).get('ROOT_UID', 0)
        if root_uid and root_uid not in admins:
            admins.update({root_uid: 'root'})
        return admins

    def check_access(self, user_id, admins=False):
        if user_id in self.access:
            return True
        root_uid = App().settings.get('TELEGRAM', {}).get('ROOT_UID', 0)
        if root_uid and user_id == root_uid:
            return True
        if admins and self.access_for_chat_admins:
            return user_id in self.get_admins_ids()
        return False

    def seen(self):
        if self.active:
            self.last_seen = datetime.datetime.now()
            self.messages += 1
            self.save()
        return self.messages

    def check_join(self, delta):
        if not self.last_join:
            return True
        return self.last_join < datetime.datetime.now() - delta

    def update_join(self):
        self.last_join = datetime.datetime.now()
        self.seen()

    def switch_activity(self, value=None):
        # FIXME: Race condition
        if value is None:
            self.active = not self.active
        else:
            self.active = value

        log.info(f"Changed active status in {self.chat_id} to {self.active}")
        self.save()

    def set_text(self, text):
        self.text = text
        log.info(f"Update text in {self.chat_id} to '{text}'")
        self.save()

    def set_join_text(self, text):
        self.join_text = text
        log.info(f"Update join text in {self.chat_id} to '{text}'")
        self.save()

    def set_min_messages(self, count: int):
        assert isinstance(count, int)

        self.min_messages = count
        log.info(f"Update minimal messages count in {self.chat_id} to '{count}'")
        self.save()


class Record(Document):
    chat_id = IntField(max_value=0)
    user_id = IntField(min_value=0)

    username = StringField()
    first_name = StringField()
    last_name = StringField()

    date = DateTimeField(default=datetime.datetime.now)
    last_seen = DateTimeField(default=datetime.datetime.now)
    last_notify = DateTimeField(default=datetime.datetime.now)

    notified = BooleanField(default=False)
    notify_count = IntField(default=0)
    ignore = BooleanField(default=False)

    messages = IntField()

    @property
    def chat(self) -> Chat:
        return Chat.get_chat(self.chat_id)

    @classmethod
    def get_record(cls, chat_id, user_id):
        match = Record.objects(chat_id=chat_id, user_id=user_id)
        if match:
            return match[0]
        return Record(chat_id=chat_id, user_id=user_id)

    @classmethod
    def write(cls, chat_id, user):
        record = Record.get_record(chat_id, user.id)

        if not record.ignore and record.chat.active:
            record.notified = False

        if not record.messages:
            record.messages = 1
        else:
            record.messages += 1

        if record.username != user.username:
            record.username = user.username or ''
        if record.first_name != user.first_name:
            record.first_name = user.first_name or ''
        if record.last_name != user.last_name:
            record.last_name = user.last_name or ''

        record.seen()

    @classmethod
    def get_from_chat(cls, chat_id):
        return Record.objects(chat_id=chat_id, notified=False, ignore=False)

    def done(self):
        self.notified = True
        self.last_notify = datetime.datetime.now()
        self.notify_count += 1
        self.messages = 0
        self.save()

    def seen(self):
        self.chat.seen()
        self.last_seen = datetime.datetime.now()
        self.save()

    def get_name(self):
        return get_username_or_name(self.user_id, self.username, self.first_name, self.last_name)

    def switch_ignore(self, value=None):
        # FIXME: Race condition
        if value is None:
            self.ignore = not self.ignore
        else:
            self.ignore = value

        log.info(f"{'Enable' if self.ignore else 'Disable'} ignoring {self.user_id} {self.get_name()} "
                 f"in {self.chat_id}")
        self.save()
