from nucleusapp_telegram.base import bot
from nucleusapp_telegram.static import ParseMode

from . import log
from .models import Chat, Record


def notify_all():
    for chat in Chat.active_chats():
        try:
            notify_in_chat(chat, min_messages=chat.min_messages)
        except Exception as e:
            log.exception(f"Can't notify chat {chat.chat_id}. Reason: '{e.__class__.__name__}: {e}'")


def notify_in_chat(chat, necessarily=False, min_messages=0):
    active = Record.get_from_chat(chat.chat_id)

    usernames = []
    messages = 0
    for record in active:
        usernames.append(record.get_name())
        messages += record.messages

    if messages < min_messages:
        return

    for record in active:
        # Reset record
        record.done()

    if not usernames and not necessarily:
        return
    if not chat.text:
        return

    try:
        bot.send_message(chat.chat_id, chat.text.replace('${USERS}', ', '.join(usernames)),
                         parse_mode=ParseMode.HTML)
        log.info(f"Notified {len(usernames)} user(s) in {chat.chat_id} for {messages} messages. {usernames}")
    except Exception as e:
        log.exception(f"Cause exception {e.__class__.__name__}: {e}")
