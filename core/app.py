from NucleusApp.modules.config import AppConfig

from app import App


class Config(AppConfig):
    def ready(self):
        from core.notify import notify_all
        App().schedule.every(30).minutes.do(notify_all)
