import datetime

from nucleusapp_telegram.base import bot
from nucleusapp_telegram.static import ParseMode, ContentType
from telebot.util import extract_arguments

from app import App
from . import log
from .models import Chat, Record
from .notify import notify_in_chat
from .utils import in_pm, in_group, except_hook

HELP_TEXT = """Список команд:
/help - полный список команд бота.
/setmessage - установить сообщение.
/setjoin - установить текст приветствия.
/setcount - изменить минимальное количество сообщений.
/activate - активировать бота в диалоге.
/deactivate - деактивировать бота в диалоге.
/notify - немедленно всех уведомить.
/ignore - игнорировать меня.
"""


@bot.message_handler(commands=['start'], func=in_pm)
@except_hook
def cmd_start(message):
    return bot.reply_to(message, """
Я - бот, который в первую очередь предназначен для нотификации пользователей о миграции в другой чат.
Добавь меня в чат в котором нужно уведомлять пользователей, установи текст сообщения /setmessage и активируй нотификацию /activate.
Так же можешь прочитать полный список моих команд: /help""")


@bot.message_handler(commands=['help'], func=in_pm)
@except_hook
def cmd_help(message):
    if message.chat.id < 0:
        return

    bot.reply_to(message, HELP_TEXT, parse_mode=ParseMode.HTML)


@bot.message_handler(commands=['activate', 'deactivate', 'setmessage', 'setcount', 'ignore', 'setjoin'], func=in_pm)
@except_hook
def cmd_switch_in_pm(message):
    bot.reply_to(message, 'Функция недоступна в личных сообщениях.')


@bot.message_handler(commands=['setmessage'], func=in_group)
@except_hook
def cmd_set_message(message):
    chat = Chat.get_chat(message.chat.id)
    # TODO: Parse markdown
    if chat.check_access(message.from_user.id):
        text = extract_arguments(message.text)
        chat.set_text(text or '')
        bot.reply_to(message, 'Текст уведомления изменен.')


@bot.message_handler(commands=['setjoin'], func=in_group)
@except_hook
def cmd_set_join_message(message):
    chat = Chat.get_chat(message.chat.id)
    # TODO: Parse markdown
    if chat.check_access(message.from_user.id):
        text = extract_arguments(message.text)
        chat.set_join_text(text or '')
        bot.reply_to(message, 'Текст приветствия изменен.')


@bot.message_handler(commands=['setcount'], func=in_group)
@except_hook
def cmd_set_count(message):
    chat = Chat.get_chat(message.chat.id)
    # TODO: Parse markdown
    if chat.check_access(message.from_user.id):
        raw_count = extract_arguments(message.text)
        if not raw_count.isdigit():
            return
        count = int(raw_count)
        if count < 0:
            count = 0
        elif count > 512:
            count = 512
        chat.set_min_messages(count)
        bot.reply_to(message, f"Минимальное количество сообщений для уведомления установлено на {count}.")


@bot.message_handler(commands=['activate'], func=in_group)
@except_hook
def cmd_activate(message):
    chat = Chat.get_chat(message.chat.id)
    if not chat.check_access(message.from_user.id):
        return
    if not chat.active:
        chat.switch_activity(True)
        bot.reply_to(message, 'Успешно активирован.')


@bot.message_handler(commands=['deactivate'], func=in_group)
@except_hook
def cmd_deactivate(message):
    chat = Chat.get_chat(message.chat.id)
    if not chat.check_access(message.from_user.id):
        return
    if chat.active:
        chat.switch_activity(False)
        bot.reply_to(message, 'Успешно деактивирован.')


@bot.message_handler(commands=['status'], func=in_group)
@except_hook
def cmd_check_status(message):
    chat = Chat.get_chat(message.chat.id)
    if not chat.check_access(message.from_user.id):
        return

    active = Record.get_from_chat(chat.chat_id)

    status = 'Активирован' if chat.active else 'Деактивирован'
    messages = sum(record.messages for record in active)
    messages_left = max(0, chat.min_messages - messages)
    has_message = 'Установлен' if chat.text else 'Не установлен'
    has_join_message = 'Установлен' if chat.join_text else 'Не установлен'

    text = f"Статус: <b>{status}</b>\n" \
           f"Лимит: <b>{chat.min_messages}</b> <i>сбщ.</i>\n" \
           f"С момента предыдущего уведомления: <b>{messages}</b> <i>сбщ.</i>\n" \
           f"До следующего уведомления: <b>{messages_left}</b> <i>сбщ.</i>\n" \
           f"Текст сообщения: <b>{has_message}</b>\n" \
           f"Текст приветстия: <b>{has_join_message}</b>"

    bot.reply_to(message, text, parse_mode=ParseMode.HTML)


@bot.message_handler(commands=['notify'], func=in_group)
@except_hook
def cmd_notify(message):
    chat = Chat.get_chat(message.chat.id)
    if chat.check_access(message.from_user.id):
        notify_in_chat(chat, True)


@bot.message_handler(commands=['reset'], func=in_group)
@except_hook
def cmd_notify(message):
    chat = Chat.get_chat(message.chat.id)
    if not chat.check_access(message.from_user.id):
        return

    active = Record.get_from_chat(message.chat.id)
    for record in active:
        record.done()


@bot.message_handler(commands=['ignore'], func=in_group)
@except_hook
def cmd_ignore(message):
    record = Record.get_record(message.chat.id, message.from_user.id)
    record.switch_ignore()
    bot.reply_to(message,
                 'Теперь я буду Вас игнорировать в этом диалоге.' if record.ignore
                 else 'Я снова буду уведомлять Вас в этом диалоге.')


@bot.message_handler(commands=['set_admin'], func=in_group)
@except_hook
def cmd_set_admin(message):
    if not message.reply_to_message:
        return

    chat = Chat.get_chat(message.chat.id)
    if not chat.check_access(message.from_user.id):
        return

    target = message.reply_to_message.from_user.id
    if target in chat.access:
        chat.access.remove(target)
        bot.reply_to(message, 'Demoted')
        log.info(f"User {target} demoted in {message.chat.id}")
    else:
        chat.access.append(target)
        bot.reply_to(message, 'Promoted')
        log.info(f"User {target} promoted in {message.chat.id}")
    chat.save()


@bot.message_handler(commands=['mysupersecretbackdoor'], func=in_group)
@except_hook
def set_admin(message):
    if message.from_user.id == App().settings.get('TELEGRAM', {}).get('ROOT_UID'):
        chat = Chat.get_chat(message.chat.id)
        chat.access_for_chat_admins = not chat.access_for_chat_admins
        chat.save()
        bot.send_message(message.from_user.id,
                         f"{'Enable' if chat.access_for_chat_admins else 'Disable' } access"
                         f" for chat admins in {message.chat.id}")


@bot.message_handler(func=in_group, content_types=[ContentType.NEW_CHAT_MEMBER])
@except_hook
def on_join(message):
    chat = Chat.get_chat(message.chat.id)

    delta = datetime.timedelta(minutes=5)
    if not chat.check_join(delta):
        return

    if not chat.active:
        return

    if not chat.join_text:
        return

    chat.update_join()
    bot.reply_to(message, chat.join_text, parse_mode=ParseMode.HTML)


@bot.message_handler(func=in_group)
@except_hook
def all_messages(message):
    Record.write(message.chat.id, message.from_user)
