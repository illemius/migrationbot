import functools

from nucleusapp_telegram.static import ContentType

from . import log


def in_pm(message):
    return message.chat.id > 0


def in_group(message):
    return message.chat.id < 0


def get_username_or_name(user_id, username=None, first_name=None, last_name=None):
    if username is None:
        name = '{} {}'.format(first_name or '', last_name or '').strip()
        return name if len(name) else user_id
    return '@' + username


def except_hook(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            log.exception(f"Exception {e.__class__.__name__}: {e}")
            return None

    return wrapper


def log_message(func):
    @functools.wraps(func)
    def wrapper(message):
        text = ''

        # Groups
        if in_group(message):
            text += f"[{message.chat.type} {message.chat.id}:{message.chat.title}"
            if message.chat.username:
                text += '@' + message.chat.username
            text += '] '

        # Sender
        text += 'from user ' + str(message.from_user.id) + ' '
        if message.from_user.username:
            text += '@' + message.from_user.username + ' '
        if message.from_user.first_name:
            text += '@' + message.from_user.first_name + ' '
        if message.from_user.last_name:
            text += '@' + message.from_user.last_name + ' '

        if message.content_type:
            text += '-> (' + message.content_type + ') '

            if message.content_type == ContentType.TEXT:
                text += '"' + message.text + '"'
            elif message.content_type == ContentType.AUDIO:
                pass

        # TODO: this

        return func(message)

    return wrapper
