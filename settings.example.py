DEBUG = True

TITLE = 'MigrationBot'

TIME_ZONE = 'Europe/Kiev'
DEFAULT_LOCALE = 'en'

# Setup MongoDB
DATABASE = {
    "active": "testing" if DEBUG else "production",
    "engines": {
        "testing": {
            "db": TITLE.lower() + "_debug_db",
        },
        "production": {
            "db": TITLE.lower() + "_db"
        }
    }
}

TELEGRAM = {
    # You can get it from @BotFather
    "TOKEN": "TOKEN",

    # You can get it from @TeleSocketBot
    "TELESOCKET_TOKEN": "TOKEN",

    # Do not change this parameter
    "TELESOCKET_MANAGER": False,

    # Skip old messages
    "SKIP_PENDING": True,

    # Author id
    "ROOT_UID": 1234567,
}
